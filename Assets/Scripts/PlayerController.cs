using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] Transform PlayerHead = null;
    [SerializeField] float mouseSensitivity = 3.7f;
    [SerializeField] float cameraAngle = 0f;
    [SerializeField] bool hideMouse = true;
    [SerializeField] float walkSpeed = 5f;
    [SerializeField] [Range(0, 0.5f)] float moveSmoothTime = 0.3f;

    private CharacterController controller = null;

    private Vector2 Direction = Vector2.zero;
    private Vector2 DirectionVelocity = Vector2.zero;


    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        if (hideMouse) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        MouseLook();
        PlayerMovement();
    }

    void MouseLook() {
        Vector2 MousePosition = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        transform.Rotate(Vector3.up * MousePosition.x * mouseSensitivity);

        cameraAngle -= MousePosition.y * mouseSensitivity;
        cameraAngle = Mathf.Clamp(cameraAngle, -90f, 90f);

        PlayerHead.localEulerAngles = Vector3.right * cameraAngle;

    }

    void PlayerMovement() {
        Vector2 movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        movementInput.Normalize();

        Direction = Vector2.SmoothDamp(Direction, movementInput, ref DirectionVelocity, moveSmoothTime);

        Vector3 velocity = (transform.forward * Direction.y + transform.right * Direction.x) * walkSpeed;

        controller.Move(velocity * Time.deltaTime);
    }
}
