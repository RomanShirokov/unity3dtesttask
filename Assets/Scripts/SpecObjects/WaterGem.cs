using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGem : MonoBehaviour, ISpecificObject
{
    [SerializeField] public string ObjectName = "WaterGem";
    [SerializeField] public GameObject waterDrop;
    [SerializeField] public float thrust = 5.0f;


    public void Trigger(Transform target) {
        Debug.Log(ObjectName);

        GameObject wd = Instantiate(waterDrop);
        wd.transform.position = gameObject.transform.position;
        Rigidbody rb = wd.gameObject.GetComponent<Rigidbody>();

        rb.AddForce(gameObject.transform.forward * thrust);
           
    }
}
