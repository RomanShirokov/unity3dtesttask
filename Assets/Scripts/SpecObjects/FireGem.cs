using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGem : MonoBehaviour, ISpecificObject
{
    [SerializeField] public string ObjectName = "FireGem";

    public float fireTime = 3.0f;
    public float timer = 0f;
    public bool active = false;

    public ParticleSystem fb;

    public void Start()
    {


    }

    public void Trigger(Transform target) {
        fb.Play(true);
        active = true;
    }

    private void Update()
    {
        if (active == true)
        {
            var em = fb.emission;
            //Debug.Log(fireTime.ToString() + " " + timer.ToString() + " " + fb.emission.enabled.ToString() + " " + active.ToString());
            em.enabled = true;

            timer = timer + Time.deltaTime;
            if (timer >= fireTime)
            {
                active = false;
                timer = 0f;
                em.enabled = false;

            }
            
        }
    }
}
