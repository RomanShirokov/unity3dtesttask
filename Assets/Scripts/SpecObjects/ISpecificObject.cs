﻿using System;
using UnityEngine;
public interface ISpecificObject
{
    public void Trigger(Transform target);
}
