using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour, ISpecificObject
{
    [SerializeField] public string ObjectName = "Pistol";
    //[SerializeField] public GameObject bulletPrefab;
    [SerializeField] public GameObject muzzleCut;
    [SerializeField] public ParticleSystem muzzleFlash;


    public void Trigger(Transform target) {
        Debug.Log(ObjectName);
        muzzleFlash.Play();

        ScareCrowController scc = target.gameObject.GetComponent<ScareCrowController>();

        scc.hit( 20.0f );
    }
}
