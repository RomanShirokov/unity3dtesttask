using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObjectController : MonoBehaviour
{
    private ISpecificObject SpecObj;

    public void Start()
    {
        SpecObj = gameObject.GetComponent<ISpecificObject>();
    }

    public void UseObject(Camera cam) {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        Physics.Raycast(ray, out hit);

        Transform endTargetTransform = hit.transform;

        SpecObj.Trigger(endTargetTransform);

    }
}
