using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScareCrowController : MonoBehaviour
{
    [SerializeField] float healthPoints = 1000.0f;
    [SerializeField] float maxHealthPoints = 1000.0f;
    [SerializeField] float wet = 100.0f;
    [SerializeField] float maxWet = 100.0f;

    [SerializeField] float fireTime = 10.0f; //sec;
    [SerializeField] float fireDamage = 5.0f; //5hp/sec
    [SerializeField] float fireCounter = 0.0f;
    float timeElapsed = 0f;
    // Start is called before the first frame update
    void Start()
    {
        HealthBarController.SetHealthBarValue(maxHealthPoints / 1000.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            FullHeal();
        }

        if (fireCounter > 0.0f)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= 1.0f)
            {
                timeElapsed = timeElapsed % 1.0f;
                FireDamage();
            }
            fireCounter -= Time.deltaTime;
            HealthBarController.SetHealthBarColor(Color.red);
        }
        else {
            fireCounter = 0.0f;
            HealthBarController.SetHealthBarColor(Color.white);
        }

        if (wet > 0) {
            HealthBarController.SetHealthBarColor(Color.blue);
        }

        HealthBarController.SetHealthBarValue(healthPoints / maxHealthPoints);

    }

    private void FireDamage() {
        healthPoints -= 5.0f;
    }

    public void FullHeal() {
        healthPoints = maxHealthPoints;
        wet = 0.0f;
        fireCounter = 0.0f;
        HealthBarController.SetHealthBarColor(Color.white);
    }

    public void Damage(float damage) {
        healthPoints -= damage;
        HealthBarController.SetHealthBarValue(HealthBarController.GetHealthBarValue() - damage);
    }

    public void hit(float damage) {
        if (wet > 0.0f) {
            damage -= 10.0f;
        }
        if (fireCounter > 0.0) {
            damage += 10.0f;
        }
        Damage(damage);
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.transform.name == "FireBurst") {

            if (wet > 0.0f) {
                wet -= 1f;
            }

            if (wet == 0f) {
                healthPoints -= 1;
                fireCounter = fireTime;
            }
        }
    }

    private void OnTriggerEnter(Collider trigger)
    {
        if (trigger.transform.name == "WaterDrop(Clone)") {
            Destroy(trigger.gameObject);

            fireCounter = 0.0f;

            wet += 10.0f;

            if (wet > 100.0f) {
                wet = 100.0f;
            }
        }
    }
}
