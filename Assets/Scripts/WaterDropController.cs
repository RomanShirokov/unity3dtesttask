using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDropController : MonoBehaviour
{
    public float lifetime = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void Awake()
    {
        lifetime = 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0) {
            Destroy(gameObject);
        }
    }


}
