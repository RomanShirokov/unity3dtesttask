using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalmController : MonoBehaviour
{
    [SerializeField] Camera cam = null;
    [SerializeField] public PalmPosition Palm;
    [SerializeField] GameObject interactiveObject;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp((int)Palm) && interactiveObject != null)
        {
            InteractiveObjectController ioc = interactiveObject.GetComponent<InteractiveObjectController>();
            ioc.UseObject(cam);
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            QFunc();
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            EFunc();
        }
    }

    void DropPickUp() {
        if (gameObject.transform.childCount > 0)
        {
            DropObject();
        }
        else
        {
            RaycastHit hit = ScanHit();
            PickUpObject(hit);
        }
    }

    void QFunc()
    {
        if(Palm == PalmPosition.Right)
        {
            //Wrong palm
            return;
        }
        DropPickUp();
    }

    void EFunc()
    {
        if (Palm == PalmPosition.Left)
        {
            //Wrong palm
            return;
        }
        DropPickUp();
    }

    RaycastHit ScanHit() {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        return hit;
    }

    private void PickUpObject(RaycastHit hit) {
        if (hit.transform.gameObject.GetComponents<InteractiveObjectController>().Length != 0 && hit.distance < 2.0f)
        {
            interactiveObject = hit.transform.gameObject;
            interactiveObject.transform.parent = gameObject.transform;
            interactiveObject.transform.position = gameObject.transform.position;
            interactiveObject.transform.rotation = gameObject.transform.rotation;
        }
    }

    private void DropObject() {
        interactiveObject.transform.parent = null;
        interactiveObject = null;
    }
}



public enum PalmPosition : int
{
    Left = 0,
    Right = 1
};
